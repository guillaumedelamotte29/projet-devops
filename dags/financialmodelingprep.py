import datetime
from datetime import timedelta
import requests
import json
import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from jsontomongo import JsonToMongoOperator

templateUrl = "https://financialmodelingprep.com/api/v3"
apiKey = "?apikey=e4d73335ebb3a2128735db2d68b221bb"

routes = {
    "profileApple": "/profile/AAPL",
    "ratingApple": "/rating/AAPL",
}


def getRequest(route):
    request = templateUrl + route
    r = requests.get(request + apiKey)
    return r.json()
